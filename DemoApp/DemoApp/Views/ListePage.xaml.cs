﻿using DemoApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListePage : ContentPage
    {


        List<Article> articles;

        public ListePage()
        {
            InitializeComponent();



            articles = new List<Article>();
            articles.Add(new Article() { Nom = "lait", Prix = "4€", Description = "Un Pack de Lait, X6" });
            articles.Add(new Article() { Nom = "Chocolat", Prix = "2€", Description = "Boite de 10" });
            articles.Add(new Article() { Nom = "Pain", Prix = "1€", Description = "Pain complet 500g" });
            articles.Add(new Article() { Nom = "Beurre", Prix = "3€", Description = "Au sel marin 250g" });

            maListView.ItemsSource = articles;




        }
    }
}