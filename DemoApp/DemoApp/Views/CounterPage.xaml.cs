﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CounterPage : ContentPage
    {

        int count = 0;


        public CounterPage()
        {
            InitializeComponent();
        }

        private void CounterClick(object sender, EventArgs e)
        {

            count++;

            countLabel.Text = count.ToString();


        }
    }
}