﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        public MenuPage()
        {
            InitializeComponent();
        }

        private void NavigateVersCounter(object sender, EventArgs e)
        {


            Navigation.PushAsync(new CounterPage());


        }

        private void NavigateVersListe(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ListePage());

        }
        private void NavigationVersTabs(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TabsPage());

        }
    }
}